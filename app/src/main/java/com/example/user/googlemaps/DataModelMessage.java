package com.example.user.googlemaps;

import java.util.Date;
import java.util.Timer;

public class DataModelMessage {
    public int id;
    public String user_id;
    public double lng;
    public double lat;
    public Date time;
    public String text;
    public double distance;


    public DataModelMessage(int id, String user_id, double lng, double lat, Date time, String text, double distance) {
        this.id = id;
        this.user_id = user_id;
        this.lng = lng;
        this.lat = lat;
        this.time = time;
        this.text = text;
        this.distance = distance;
    }

    public int getId() {
        return id;
    }

    public String getUser_id() {
        return user_id;
    }

    public double getLng() {
        return lng;
    }

    public double getLat() {
        return lat;
    }

    public Date getTime() {
        return time;
    }

    public String getText() {
        return text;
    }

    public double getDistance() {
        return distance;
    }

    @Override
    public String toString() {
        return "DataModelMessage{" +
                "user_id='" + user_id + '\'' +
                ", lat=" + lat +
                ", lng=" + lng +
                ", text='" + text + '\'' +
                ", time=" + time +
                '}';
    }
}