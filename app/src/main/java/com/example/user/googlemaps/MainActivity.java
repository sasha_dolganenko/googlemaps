package com.example.user.googlemaps;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {
    public static final String USER_NAME = "Name";
    final List<DataModelMessage> messageList = new ArrayList<DataModelMessage>();
    final LatLng KHARKOV = new LatLng(49.9944422, 36.2368201);
    MessageAdapter messageAdapter;
    private DataModelMessage dataModelMessage;
    private LatLng currentLatLng;
    private ImageButton sendButton;
    private ImageButton openListButton;
    private EditText messageEditText;
    private ListView listOfMessages;
    private LocationManager locationManager;
    private Marker marker;
    private String userName;
    private GoogleMap map;
    private Location location;

    private ImageButton btt1;
    private ImageButton btt2;
    private ImageButton btt3;

    public static String enteredMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        userName = getIntent().getStringExtra(LoginActivity.KEY_USER_NAME);

        messageAdapter = new MessageAdapter(getApplicationContext(), messageList);
        messageEditText = (EditText) findViewById(R.id.edit_text_search);
        btt1 = (ImageButton) findViewById(R.id.first_btt);
        btt1.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    }
                }
        );
        btt2 = (ImageButton) findViewById(R.id.second_btt);
        btt2.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                    }
                }
        );
        btt3 = (ImageButton) findViewById(R.id.third_btt);
        btt3.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                    }
                }
        );
//        new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(getApplicationContext(), "something", Toast.LENGTH_SHORT).show();
//                switch (v.getId()) {
//                    case R.id.first_btt:
//                        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//                        break;
//                    case R.id.second_btt:
//                        map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
//                        break;
//                    case R.id.third_btt:
//                        map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
//                        break;
//                }
//            }
//        };


        messageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                enteredMessage = messageEditText.getText().toString();
            }
        });

        sendButton = (ImageButton) findViewById(R.id.button_send);
        sendButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (MainActivity.this.location == null) {
                    Toast.makeText(getApplicationContext(), "please wait...", Toast.LENGTH_SHORT).show();
                } else {
                    Retrofit.sendMessage(userName, location.getLongitude(), location.getLatitude(), messageEditText.getText().toString(), new Callback<DataModelMessage>() {
                        @Override
                        public void success(DataModelMessage dataModelMessage, Response response) {
                            if (map != null) {
//                                map.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(),
//                                        location.getLongitude())).title(userName + ": " +
//                                        messageEditText.getText().toString() +
//                                        "(" +
                                Retrofit.getMessage(location.getLongitude(), location.getLatitude(), new Callback<List<DataModelMessage>>() {
                                    @Override
                                    public void success(List<DataModelMessage> dataModelMessages, Response response) {
                                        createPins(dataModelMessages);
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {

                                    }
                                });
//                                +
//                                        ")"));
                                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15));
                                map.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
                                messageEditText.setText("");
                            } else {
                                Toast.makeText(getApplicationContext(), "map = null", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }
        });


//        listOfMessages = (ListView) findViewById(R.id.list_view_messages);
//        listOfMessages.setVisibility(View.GONE);
        openListButton = (ImageButton) findViewById(R.id.clear_all);
        openListButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        map.clear();
                    }
                }
        );
    }

    private void createPins(List<DataModelMessage> dataModelMessages) {
        for (DataModelMessage dataModelMessage : dataModelMessages) {
            map.addMarker(new MarkerOptions().position(new LatLng(dataModelMessage.getLat(),
                    dataModelMessage.getLng())).title(dataModelMessage.getUser_id() + ": " +
                    dataModelMessage.getText() +
                    "(" + df.format(dataModelMessage.getTime()) + ")"));
        }

    }

    DateFormat df = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());


    @Override
    public void onMapReady(GoogleMap map) {

    }

    @Override
    protected void onResume() {
        super.onResume();

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5 * 1000, 10, locationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5 * 1000, 10, locationListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(locationListener);
    }

    private LocationListener locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            MainActivity.this.location = location;

        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }
    };


}
