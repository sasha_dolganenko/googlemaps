package com.example.user.googlemaps;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

public class Retrofit {

    private static final String ENDPOINT = "http://psi.kh.ua/hakaton";
    private static ApiInterface apiInterface;

    interface ApiInterface {

        @FormUrlEncoded
        @POST("/api.php")
        void sendMessage(@Field("user_id") String user_id,
                         @Field("lng") double lng,
                         @Field("lat") double lat,
                         @Field("text") String text,
                         Callback<DataModelMessage> callback);


        @GET("/api.php")
        void getMessage(@Query("lng") double lng,
                        @Query("lat") double lat,
                        Callback<List<DataModelMessage>> callback);


    }

    static {
        initialize();
    }

    public static void initialize() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }


    public static void sendMessage(String user_id, double lng, double lat, String text, Callback<DataModelMessage> callback) {
        apiInterface.sendMessage(user_id, lng, lat, text, callback);

    }

    public static void getMessage(double lng, double lat, Callback<List<DataModelMessage>> callback) {
        apiInterface.getMessage(lng, lat, callback);
    }

}
