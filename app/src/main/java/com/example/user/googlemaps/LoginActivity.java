package com.example.user.googlemaps;


import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.FileInputStream;

public class LoginActivity extends AppCompatActivity {
    public static final String KEY_USER_NAME = "user_name";
    private EditText userName;
    private EditText password;
    private Button loginButton;
    private Button orRegisterButton;


    public static String loginData;
    private String passwordData;

    private String enteredLogin;
    private String enteredPassword;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        userName = (EditText) findViewById(R.id.username_edit_text);
        password = (EditText) findViewById(R.id.password_edit_text);
        loginButton = (Button) findViewById(R.id.login_button);
        orRegisterButton = (Button) findViewById(R.id.or_register_button);
        takeRegisteredInfo();

        loginButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        enteredLogin = userName.getText().toString();
                        enteredPassword = password.getText().toString();

                        if (isPasswordAndLoginMatching()) {
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.putExtra(KEY_USER_NAME, loginData);
                            userName.setText("");
                            password.setText("");
                            startActivity(intent);
                            finish();
                        } else {
                            final AlertDialog.Builder wrongPassAlert = new AlertDialog.Builder(LoginActivity.this);
                            wrongPassAlert.setTitle("ERROR");
                            wrongPassAlert.setMessage("Wrong password or login! Try retyping");
                            wrongPassAlert.show();
                        }
                    }
                }
        );

        orRegisterButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                        startActivity(intent);
                    }
                }
        );
    }

    private void takeRegisteredInfo() {
        try {
            byte[] buffer = new byte[100000];
            FileInputStream inputStream = openFileInput(RegisterActivity.fileName);
            int length = inputStream.read(buffer);
            inputStream.close();
            String fileContent = new String(buffer, 0, length);
            String[] lines = fileContent.split("\n");
            loginData = lines[0];
            passwordData = lines[1];
        } catch (Exception ignore) {

        }
    }

    private boolean isPasswordAndLoginMatching() {
        if (enteredLogin.equals(loginData) && enteredPassword.equals(passwordData)) {
            return true;
        } else {
            return false;
        }

    }
}
