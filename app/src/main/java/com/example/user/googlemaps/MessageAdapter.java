package com.example.user.googlemaps;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class MessageAdapter extends ArrayAdapter<DataModelMessage> {

    public MessageAdapter(Context context, List<DataModelMessage> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View itemView = convertView;
        if (itemView == null) {
            itemView = View.inflate(getContext(), R.layout.one_message_item, null);
        }
        TextView userName = (TextView) convertView.findViewById(R.id.text_view_user_name);
        TextView userMessage = (TextView) convertView.findViewById(R.id.text_view_user_message);
        TextView timeSent = (TextView) convertView.findViewById(R.id.text_view_time_sent);
        DataModelMessage dataModelMessage = getItem(position);

        userName.setText(dataModelMessage.getUser_id());
        userMessage.setText(MainActivity.enteredMessage);
        timeSent.setText(dataModelMessage.getTime().toString());
        return itemView;
    }
}
