package com.example.user.googlemaps;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileOutputStream;

public class RegisterActivity extends AppCompatActivity {
    private EditText password;
    private EditText confirmPassword;
    private EditText login;
    private Button registerButton;

    public static String fileName = "userLoginInfo";

    private String passwordSave;
    private String confirmPasswordSave;
    private String loginSave;
    FileOutputStream outputStream;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        password = (EditText) findViewById(R.id.password_register_edit_text);
        confirmPassword = (EditText) findViewById(R.id.password_register_confirm_edit_text);
        login = (EditText) findViewById(R.id.username_register_edit);
        registerButton = (Button) findViewById(R.id.register_button_register_activity);

        final AlertDialog.Builder blankFieldsAlert = new AlertDialog.Builder(this);
        blankFieldsAlert.setTitle("ERROR");
        blankFieldsAlert.setMessage("You can't leave blank fields!");

        final AlertDialog.Builder wrongPassAlert = new AlertDialog.Builder(this);
        wrongPassAlert.setTitle("ERROR");
        wrongPassAlert.setMessage("Wrong password confirmation! Try retyping");

        registerButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (login.getText().length() > 0
                                && password.getText().length() > 0
                                && confirmPassword.getText().length() > 0) {

                            loginSave = login.getText().toString();
                            passwordSave = password.getText().toString();
                            confirmPasswordSave = confirmPassword.getText().toString();

                        } else {
                            blankFieldsAlert.show();
                        }

                        try {
                            if (passwordSave.equals(confirmPasswordSave)) {
                                outputStream = openFileOutput(fileName, Context.MODE_PRIVATE);
                                outputStream.write(loginSave.getBytes());
                                outputStream.write("\n".getBytes());
                                outputStream.write(passwordSave.getBytes());
                                outputStream.write("\n".getBytes());
                                outputStream.close();
                                Toast saved = Toast.makeText(getApplicationContext(),
                                        "Registered!", Toast.LENGTH_SHORT);
                                saved.show();

                                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                wrongPassAlert.show();
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
        );

    }

}
